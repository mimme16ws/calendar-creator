(function () {
    "use strict";
    /* eslint-env node */

    var http = require("http");
    var path = require("path");
    var cors = require("cors");
    var webrtc = require("webrtc.io");
    var express = require("express");
    var app = express();

    var that = {};


    function initWebServer(port, pathToClientDirectory) {
        var WWW = path.join(__dirname, pathToClientDirectory);
        var expressServer = express();
        expressServer.use(cors());
        expressServer.use(express.static(WWW));
        return http.createServer(expressServer).listen(port);
    }

    function initStreamRelay(server) {
        webrtc.listen(server);
    }


    function parsePortArguments() {
        var port = parseInt(process.argv[2]);
        if (!isNaN(port)) {
            return port;
        } else {
            throw "Invalid arguments exception. Start server with 'node app.js WEBPORT";
        }
    }

    function run() {
        var port = parsePortArguments(),
            server;

        server = initWebServer(port, "../www/");
        initStreamRelay(server);

    }

    that.run = run;
    return that;
}().run());