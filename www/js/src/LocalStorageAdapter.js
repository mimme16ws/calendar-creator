var CalendarCreator = CalendarCreator || {};
CalendarCreator.LocalStorageAdapter = function () {
    "use strict";
    /* eslint-env browser */
    var that = {};

    function save(key, data) {
        localStorage.setItem(key, JSON.stringify(data));
    }

    function restore(key) {
        var data = JSON.parse(localStorage.getItem(key)) || {};
        return data;
    }
    
    function getItem(key) {
        if (localStorage.getItem(key) !== null) {
            return true;
        }
        return false;
    }
    

    that.save = save;
    that.restore = restore;
    that.getItem = getItem;
    return that;
};