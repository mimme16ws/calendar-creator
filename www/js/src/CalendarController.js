var CalendarCreator = CalendarCreator || {};
CalendarCreator.CalendarController = function (module) {
    "use strict";
    /* eslint-env browser  */

    //takes care of Canvas-manipulation, gets information from module

    var that = new window.EventPublisher(),

        calendarObject,

        myDays = [],
        restDays = [],
        days,
        monthArray = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
        begin,
        rows,
        rest,

        canvas = document.getElementById("calendar"),
        context,
        canvasWidth = 841,
        canvasHeight = 841 * Math.sqrt(2), /*DIN-A5 - Width * sqrt(2) = DIN-A5-Height*/
        image,
        x,
        sourceX,
        sourceY,
        sourceWidth,
        sourceHeight,
        imageScaledWidth,
        imageScaledHeight,

        positioningOffset,
        orientationOffset,
        resolution = 1,


        marginSide,
        marginGrid,
        gridWidth,
        gridHeight,
        xR,
        yR,
        aR,
        bR,
        xF,
        right,
        numWidth,
        weekday;

    function drawImage(img, x, y, a, b) {
        if (img) {
            context.drawImage(img, x, y, a, b);
        }
    }

    function fillArrays() {
        var k, n, i, l, counterDaysNextMonth;
        myDays = []; //actual days of curren months
        restDays = []; // days before and after, to fill up grid
        days = calendarObject.days;
        begin = calendarObject.begin;
        rows = parseInt((days + begin) / 7, 10);
        rest = days + begin - rows * 7;
        //myDays
        for (k = 0; k < rows; k++) {
            for (n = 0; n < 7; n++) {
                myDays.push([n, k]);
            }
        }
        for (i = 0; i < begin; i++) {
            myDays.shift();

        }
        for (i = 0; i < rest; i++) {
            myDays.push([i, k]);
        }

        //restDays
        //previous month
        l = begin;
        for (i = 0; i < begin; i++) {
            l -= 1;
            restDays.push([i, 0, calendarObject.daysPrevious - l]);

        }
        //next month
        counterDaysNextMonth = 1;
        if (rest !== 0 && rows === 4) {
            for (i = rest; i < 7; i++) {
                restDays.push([i, 4, counterDaysNextMonth]);
                counterDaysNextMonth += 1;
            }
            for (i = 0; i < 3; i++) {
                restDays.push([i, 5, counterDaysNextMonth]);
                counterDaysNextMonth += 1;
            }
        } else {
            for (i = rest; i < 3; i++) {
                restDays.push([i, 5, counterDaysNextMonth]);
                counterDaysNextMonth += 1;
            }
        }
    }

    function checkMarkedDays(currentDay) {
        return calendarObject.markDays.some(function (markedDay) {
            return currentDay === markedDay;
        });
    }

    function drawGridRect() {
        var i, position, xPosition, yPosition;

        if (calendarObject.fillUpGrid === true) {
            for (i = 0; i < restDays.length; i++) {
                position = restDays[i];
                xPosition = position[0] * gridWidth / 7;
                yPosition = position[1] * gridHeight / 6;
                numWidth = context.measureText(position[2].toString()).width;

                context.strokeStyle = calendarObject.gridColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.4)");
                //context.strokeStyle = shadeRGBColor(calendarObject.gridColor, 0.4);
                context.strokeRect(xPosition + xR, yPosition + yR, aR, bR);

                context.fillStyle = calendarObject.fontColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.5)");
                context.fillText(position[2].toString(), xPosition + xF - numWidth * right, yPosition + yR + 30 * resolution);
            }
        }

        for (i = 0; i < myDays.length; i++) {
            position = myDays[i];
            xPosition = position[0] * gridWidth / 7;
            yPosition = position[1] * gridHeight / 6;
            numWidth = context.measureText((i + 1).toString()).width;

            if (checkMarkedDays(i + 1)) {
                context.fillStyle = calendarObject.highlightColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.5)");
                context.fillRect(xPosition + xR, yPosition + yR, aR, bR);
            }
            context.strokeStyle = calendarObject.gridColor;
            context.strokeRect(xPosition + xR, yPosition + yR, aR, bR);

            context.fillStyle = calendarObject.fontColor;
            context.fillText((i + 1).toString(), xPosition + xF - numWidth * right, yPosition + yR + 30 * resolution);
        }
    }


    function drawGridLine() {
        var i, position, xPosition, yPosition;

        if (calendarObject.fillUpGrid === true) {
            for (i = 0; i < restDays.length; i++) {
                position = restDays[i];
                xPosition = position[0] * gridWidth / 7;
                yPosition = position[1] * gridHeight / 6;
                context.strokeStyle = calendarObject.gridColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.4)");

                context.beginPath();
                context.moveTo(xPosition + xR, yPosition + yR);
                context.lineTo(xPosition + xR, yPosition + bR);
                context.lineTo(xPosition + aR, yPosition + bR);
                context.stroke();

                context.fillStyle = calendarObject.fontColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.5)");
                context.fillText(position[2].toString(), xPosition + xF, yPosition + yR + 30 * resolution);
            }
        }

        for (i = 0; i < myDays.length; i++) {
            position = myDays[i];
            xPosition = position[0] * gridWidth / 7;
            yPosition = position[1] * gridHeight / 6;

            if (checkMarkedDays(i + 1)) {
                context.strokeStyle = calendarObject.highlightColor;
            } else {
                context.strokeStyle = calendarObject.gridColor;
            }

            context.beginPath();
            context.moveTo(xPosition + xR, yPosition + yR);

            context.lineTo(xPosition + xR, yPosition + bR);
            context.lineTo(xPosition + aR, yPosition + bR);
            /*********************************************
            MORE GRID OPTIONS FOR LATER USE
            *********************************************/
            
            //context.moveTo( xPosition+ xR, yPosition + yR);
            //context.lineTo(xPosition + aR, yPosition + yR);
            //context.lineTo(xPosition + xR, yPosition + bR);
            //context.lineTo(xPosition + xR, yPosition + yR);

            //context.moveTo( xPosition+ aR, yPosition + yR);

            //context.lineTo(xPosition + aR, yPosition + bR);
            //context.lineTo(xPosition + xR, yPosition + bR);

            //context.lineTo(xPosition + xR, yPosition + yR);
            //context.lineTo(xPosition + xR, yPosition + bR);

            //context.moveTo( xPosition+ xR, yPosition + yR);

            //context.lineTo(xPosition + aR, yPosition + yR);
            //context.lineTo(xPosition + aR, yPosition + bR);

            context.stroke();
            context.fillStyle = calendarObject.fontColor;
            context.fillText((i + 1).toString(), xPosition + xF, yPosition + yR + 30 * resolution);
        }
    }

    function drawGridArc() {
        var i, position, xPosition, yPosition, arcStart, arcEnd, numWidth, radius = 40;

        arcStart = 45 * Math.PI / 180; //gapOnOtherSide: 225*Math.PI/180   eventuell ganze Kreise; evtl Lücke kleiner machen  45 Hochformat: 45 / Querformat: 45
        arcEnd = (315 - 110 * orientationOffset) * Math.PI / 180; //gapOnOtherSide: 495*Math.PI/180  315  Hochformat: 315; Querformat 205

        if (calendarObject.fillUpGrid === true) {
            for (i = 0; i < restDays.length; i++) {
                position = restDays[i];
                xPosition = position[0] * gridWidth / 7;
                yPosition = position[1] * gridHeight / 6;

                context.beginPath();
                context.arc(xPosition + xR, yPosition + yR, radius * resolution, arcStart, arcEnd, false);
                context.strokeStyle = calendarObject.gridColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.4)");
                context.stroke();

                numWidth = context.measureText(position[2].toString()).width;
                context.fillStyle = calendarObject.fontColor.replace(/rgb/i, "rgba").replace(/\)/i, ", 0.5)");
                context.fillText(position[2].toString(), xPosition + xR - numWidth / 2, yPosition + yR + 10 * resolution);
            }
        }


        for (i = 0; i < myDays.length; i++) {
            position = myDays[i];
            xPosition = position[0] * gridWidth / 7;
            yPosition = position[1] * gridHeight / 6;

            if (checkMarkedDays(i + 1)) {
                context.strokeStyle = calendarObject.highlightColor;
            } else {
                context.strokeStyle = calendarObject.gridColor;
            }

            context.beginPath();
            context.arc(xPosition + xR, yPosition + yR, radius * resolution, arcStart, arcEnd, false);
            context.stroke();

            numWidth = context.measureText((i + 1).toString()).width;
            context.fillStyle = calendarObject.fontColor;
            context.fillText((i + 1).toString(), xPosition + xR - numWidth / 2, yPosition + yR + 10 * resolution);

        }
    }

    function createWeekdayBar() {
        var weekdays = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            i,
            xPosition,
            yPosition,
            stringWidth;
        if (calendarObject.uppercase) {
            weekdays = ["MO", "DI", "MI", "DO", "FR", "SA", "SO"];
        }
        if (calendarObject.lowercase) {
            weekdays = ["mo", "di", "mi", "do", "fr", "sa", "so"];
        }

        for (i = 0; i < weekdays.length; i++) {
            xPosition = (i + 1) * gridWidth / 7 - gridWidth / 14 + marginSide;
            yPosition = canvasHeight / 2 + 30 * resolution;
            context.font = calendarObject.fontVariant + calendarObject.fontStyle + (25 * resolution).toString() + "px " + calendarObject.font;
            stringWidth = context.measureText(weekdays[i].toString()).width;
            context.fillText(weekdays[i].toString(), xPosition - stringWidth / 2, yPosition - canvasHeight / 2 * positioningOffset);
        }
    }

    function createNewGrid() {
        fillArrays();
        weekday = calendarObject.weekday;

        if (calendarObject.gridDesign === 3 || calendarObject.gridDesign === 4) {
            marginSide = 10 * resolution;
            marginGrid = 7 * resolution;
            gridWidth = canvasWidth - marginSide * 2;
            gridHeight = canvasHeight / 2 - marginSide * 2 - 30 * weekday * resolution;
        } else {
            marginSide = 12.5 * resolution;
            marginGrid = 7 * resolution;
            gridWidth = canvasWidth - marginSide * 2.5;
            gridHeight = canvasHeight / 2 - marginSide * 2.5 - 30 * weekday * resolution;
        }

        context.lineWidth = 3 * resolution;
        context.font = calendarObject.fontVariant + calendarObject.fontStyle + (30 * resolution).toString() + "px " + calendarObject.font;
        context.strokeStyle = calendarObject.gridColor;

        //switch between the grid designs; adjust values
        switch (calendarObject.gridDesign) {
        case 1:
            xR = marginSide + marginGrid / 2;
            yR = canvasHeight / 2 + marginSide + marginGrid / 2 + 30 * resolution * weekday - canvasHeight / 2 * positioningOffset;
            aR = gridWidth / 7;
            bR = gridHeight / 6;
            xF = xR + 5 * resolution;
            right = 0;
            drawGridRect();
            if (weekday === 1) {
                createWeekdayBar();
            }
            break;


        case 2:
            xR = marginSide + marginGrid / 2;
            yR = canvasHeight / 2 + marginSide + marginGrid / 2 + 30 * resolution * weekday - canvasHeight / 2 * positioningOffset;
            aR = gridWidth / 7;
            bR = gridHeight / 6;
            xF = aR + 5 * resolution;
            right = 1;
            drawGridRect();
            if (weekday === 1) {
                createWeekdayBar();
            }
            break;

        case 3:
            xR = marginSide + marginGrid / 2;
            yR = canvasHeight / 2 + marginSide + marginGrid / 2 + 30 * resolution * weekday - canvasHeight / 2 * positioningOffset;
            aR = gridWidth / 7 - marginGrid;
            bR = gridHeight / 6 - marginGrid;
            xF = xR + 5 * resolution;
            right = 0;
            drawGridRect();
            if (weekday === 1) {
                createWeekdayBar();
            }
            break;

        case 4:
            xR = marginSide + marginGrid / 2;
            yR = canvasHeight / 2 + marginSide + marginGrid / 2 + 30 * resolution * weekday - canvasHeight / 2 * positioningOffset;
            aR = gridWidth / 7 - marginGrid;
            bR = gridHeight / 6 - marginGrid;
            xF = aR + 5 * resolution;
            right = 1;
            drawGridRect();
            if (weekday === 1) {
                createWeekdayBar();
            }
            break;

        case 5:
            xR = marginSide + marginGrid / 2 + 10 * resolution;
            yR = canvasHeight / 2 + marginSide + marginGrid / 2 + 30 * resolution * weekday - canvasHeight / 2 * positioningOffset;
            aR = gridWidth / 7 - marginGrid + 10 * resolution;
            bR = canvasHeight / 2 + marginGrid / 2 + gridHeight / 6 + 20 * resolution * weekday - canvasHeight / 2 * positioningOffset;
            xF = xR + 5 * resolution;
            drawGridLine();
            if (weekday === 1) {
                createWeekdayBar();
            }
            break;

        case 6:
            xR = marginSide + marginGrid / 2 + gridWidth / 7 / 2;
            yR = canvasHeight / 2 + marginSide + marginGrid / 2 + gridHeight / 6 / 2 + 30 * resolution * weekday - 10 * resolution * orientationOffset - canvasHeight / 2 * positioningOffset; //Hochformat: yR; Querformat yR -10
            aR = marginSide + marginGrid / 2;
            bR = canvasHeight / 2 + marginSide + marginGrid / 2 + gridHeight / 6;
            xF = xR + 5 * resolution;
            drawGridArc();
            if (weekday === 1) {
                createWeekdayBar();
            }
            break;

        default:
        }
    }



    function createNewMonth() {
        //writes months on canvas
        var monthString = monthArray[calendarObject.month],
            textX,
            textY;
        if (calendarObject.uppercase) {
            monthString = monthString.toUpperCase();
        }

        if (calendarObject.lowercase) {
            monthString = monthString.toLowerCase();
        }


        context.font = calendarObject.fontVariant + calendarObject.fontStyle + (60 * resolution).toString() + "px " + calendarObject.font;
        textX = canvasWidth - context.measureText(monthString).width - 35 * resolution;
        textY = canvasHeight - 35 * resolution + 15 * orientationOffset * resolution - canvasHeight / 2 * positioningOffset;
        context.fillStyle = calendarObject.fontColor;
        context.fillText(monthString, textX, textY); //Hochformat:  Querformat +15 (60 * resolution).toString() + " "
    }


    function setImageValues(imageX, imageY, imageContainerWidth, imageContainerHeight) {
        //wenn image-format kleiner container-format.. (verhindert, dass bildbreite bei "kleinerem" format verkleinert wird)
        if (imageX > imageY && imageX / imageY < imageContainerWidth / imageContainerHeight) {
            //wenn image-format kleiner container-format.. (verhindert, dass bildbreite bei "kleinerem" format verkleinert wird)
            x = imageX / imageContainerWidth;
        } else {
            x = imageY / imageContainerHeight;
        }
        //default
        imageScaledWidth = imageContainerWidth;
        imageScaledHeight = imageContainerHeight;
        sourceWidth = imageContainerWidth * x;
        sourceHeight = imageContainerHeight * x;
        //handles rotation
        if ((calendarObject.rotationValue / 90) % 2 !== 0) {
            if (imageX > imageY && calendarObject.orientation === "portrait") {
                imageScaledWidth = imageContainerHeight;
                imageScaledHeight = imageContainerHeight * imageContainerHeight / imageContainerWidth;
            } else {
                x = imageY / imageContainerWidth;
                imageScaledWidth = imageContainerHeight;
                imageScaledHeight = imageContainerWidth;

                if (imageContainerWidth / imageContainerHeight > imageY / imageX && imageContainerWidth / (imageContainerHeight + 100) < imageY / imageX) {
                    // +100 wenn nur kleiner links und rechts dann seiten zuschneiden
                    sourceWidth = imageScaledWidth * imageY / imageScaledHeight;
                    sourceHeight = imageScaledHeight * imageY / imageScaledHeight;

                } else {

                    sourceWidth = imageScaledWidth * imageX / imageScaledWidth;
                    sourceHeight = imageScaledHeight * imageX / imageScaledWidth;
                }
            }

        }

        sourceX = (imageX - sourceWidth) / 2;
        sourceY = (imageY - sourceHeight) / 2;

    }

    function createNewImage() {

        image = new Image();
        image.src = calendarObject.imageSrc;

        image.onload = function () {
            var margin = 12.5 * resolution,
                imageContainerWidth = canvasWidth - 2 * margin,
                imageContainerHeight = canvasHeight / 2 - margin;


            setImageValues(image.width, image.height, imageContainerWidth, imageContainerHeight);
            context.save();
            context.translate((imageContainerWidth / 2 + margin), (imageContainerHeight / 2 + margin + (canvasHeight / 2 - margin) * positioningOffset));
            context.rotate(calendarObject.rotationValue * Math.PI / 180);
            //sourceX, sourceY, sourceWidth, sourceHeight -> crops image
            context.drawImage(image, sourceX, sourceY, sourceWidth, sourceHeight, -imageScaledWidth / 2, -imageScaledHeight / 2, imageScaledWidth, imageScaledHeight);
            context.restore();


        };

    }

    function drawCanvas() {
        context = canvas.getContext("2d");
        canvas.setAttribute("width", canvasWidth);
        canvas.setAttribute("height", canvasHeight);
    }

    function clearCanvas() {
        context.beginPath();
        context.save();
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.clearRect(0, 0, canvasWidth, canvasHeight);
        context.restore();
    }

    function createBackground() {
        var image = new Image();

        image.src = calendarObject.background;
        image.onload = function () {
            var numHorizontal = parseInt(canvasWidth / image.width * resolution, 10) + 1,
                numVertical = parseInt(canvasHeight / image.height * resolution, 10) + 1,
                i,
                k,
                imageWidth = image.width * resolution,
                imageHeight = image.height * resolution;

            for (i = 0; i < numHorizontal; i++) {
                for (k = 0; k < numVertical; k++) {
                    drawImage(image, i * imageWidth, k * imageHeight, imageWidth, imageHeight);
                }

            }
            //create background before rest
            createNewGrid();
            createNewMonth();
            createNewImage();
        };
    }

//landscape ore portrait + resolution options for canvas
    function setPositioningValues() {
        var positioning = calendarObject.positioning,
            orientation = calendarObject.orientation,
            cssWidth,
            cssHeight;

        if (positioning === "imageTop") {
            positioningOffset = 0;
        }

        if (positioning === "imageBottom") {
            positioningOffset = 1;
        }

        if (orientation === "portrait") {
            canvasWidth = 841 * resolution;
            canvasHeight = 841 * Math.sqrt(2) * resolution;
            cssWidth = 841 * 0.5 * 100 / document.documentElement.clientHeight;
            cssHeight = 841 * Math.sqrt(2) * 0.5 * 100 / document.documentElement.clientHeight;
            window.$("canvas").css("width", cssWidth.toString() + "vh");
            window.$("canvas").css("height", cssHeight.toString() + "vh");
            orientationOffset = 0;
        }

        if (orientation === "landscape") {
            canvasWidth = 841 * Math.sqrt(2) * resolution;
            canvasHeight = 841 * resolution;
            cssWidth = 841 * Math.sqrt(2) * 0.65 * 100 / document.documentElement.clientHeight;
            cssHeight = 841 * 0.65 * 100 / document.documentElement.clientHeight;
            window.$("canvas").css("width", cssWidth.toString() + "vh");
            window.$("canvas").css("height", cssHeight.toString() + "vh");
            orientationOffset = 1;
        }
    }

    function update() {
        calendarObject = module.getCalendarObject();

        resolution = calendarObject.resolution;
        setPositioningValues();
        clearCanvas();

        drawCanvas();
        createBackground();


    }

    function print() {
        var hiddenCanvas = canvas.toDataURL("image/png", 1.0);
        window.$("#hiddenCanvas").attr("src", hiddenCanvas);
        window.$("#hiddenCanvas").printElement({
            printMode: "popup"
        });
    }



    function getCanvasAsImage(x, y, width, height, fileType) {
        var imgData = context.getImageData(x, y, width, height);
        var hiddenCanvas = document.createElement("canvas");
        var hiddenContext;
        hiddenCanvas.width = imgData.width;
        hiddenCanvas.height = imgData.height;
        hiddenContext = hiddenCanvas.getContext("2d");
        hiddenContext.putImageData(imgData, 0, 0);

        return hiddenCanvas.toDataURL("image/" + fileType);
    }

    function downloadURI(link, dataUrl, name) {
        link.download = name;
        link.href = dataUrl;
    }


    function download(event) {
        var link = event.data[0],
            fileType = event.data[1];

        downloadURI(link, getCanvasAsImage(0, 0, canvasWidth, canvasHeight, fileType), monthArray[calendarObject.month] + "-" + calendarObject.year.toString() + "." + fileType);

    }

    function init() {
        calendarObject = module.getCalendarObject();
        drawCanvas();
        return that;
    }

    that.update = update;
    that.print = print;
    that.download = download;
    that.init = init;

    return that;
};