var CalendarCreator = CalendarCreator || {};
CalendarCreator = (function () {
    "use strict";
    /* eslint-env browser  */
    var that = {},
        module,
        timeSelector,
        menuSelector,
        calendarController,
        dropTarget;

    function initEvents() {
        timeSelector.addEventListener("yearChanged", module.setTimeValues);
        timeSelector.addEventListener("monthChanged", module.setTimeValues);
        calendarController.addEventListener("downloadMonth", module.setTimeValues);

        menuSelector.addEventListener("fontChanged", module.setFontValues);
        menuSelector.addEventListener("fontVariantChanged", module.setFontValues);
        menuSelector.addEventListener("fontStyleChanged", module.setFontValues);
        menuSelector.addEventListener("fontUppercaseChanged", module.setFontValues);
        menuSelector.addEventListener("fontLowercaseChanged", module.setFontValues);

        menuSelector.addEventListener("positioningChanged", module.setPositioning);
        menuSelector.addEventListener("orientationChanged", module.setOrientation);

        menuSelector.addEventListener("colorChanged", module.setColors);
        menuSelector.addEventListener("backgroundChanged", module.setBackground);
        menuSelector.addEventListener("imageChanged", module.setImage);
        menuSelector.addEventListener("newFileImage", module.setImageFromFile);
        menuSelector.addEventListener("newHTMLImage", module.setImageFromHTML);
        menuSelector.addEventListener("rotateLeft", module.setRotation);

        menuSelector.addEventListener("gridChanged", module.setGrid);
        menuSelector.addEventListener("weekdaysChanged", module.setWeekdays);
        menuSelector.addEventListener("fillUpGridChanged", module.setFillUpGrid);
        
        menuSelector.addEventListener("resolutionChanged", module.setResolution);
        menuSelector.addEventListener("highlightedDaysChanged", module.setMarkedDays);
        menuSelector.addEventListener("print", calendarController.print);
        menuSelector.addEventListener("download", calendarController.download);
        
        module.addEventListener("update", calendarController.update);
        module.addEventListener("setDefaultTimeUI", timeSelector.setDefaultTimeUI);
        module.addEventListener("setHighlightUI", menuSelector.createSubmenuHighlight);
        module.addEventListener("updateUI", menuSelector.updateUI);

        dropTarget.addEventListener("fileDropped", module.onFileDropped);
    }



    function init() {
        
        module = CalendarCreator.Module();
        timeSelector = CalendarCreator.TimeSelector();
        menuSelector = CalendarCreator.MenuSelector();
        calendarController = CalendarCreator.CalendarController(module);
        dropTarget = new CalendarCreator.DropTarget(document.querySelector(".canvas"));

        initEvents();

        timeSelector.init();
        menuSelector.init();
        calendarController.init();
        module.init();


    }

    that.init = init;
    return that;
}());