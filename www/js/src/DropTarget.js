var CalendarCreator = CalendarCreator || {};
CalendarCreator.DropTarget = function (targetNode) {
    "use strict";
    /* EventPublisher */
    
    // CalendarCreator.DropTarget takes care of drag and drop events;
    
    var that = new window.EventPublisher();
    
    function onDragOver(event) {
        targetNode.classList.add("dragging");
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = "copy";
    }

    function onDragLeft() {
        targetNode.classList.remove("dragging");
    }

    function onFileDropped(event) {
        targetNode.classList.remove("dragging");
        event.stopPropagation();
        event.preventDefault();
        that.notifyAll("fileDropped", event.dataTransfer.files[0]);
    }

    function show() {
        targetNode.classList.remove("hidden");
    }

    function hide() {
        targetNode.classList.add("hidden");
    }

    function init() {
        targetNode.addEventListener("dragover", onDragOver);
        targetNode.addEventListener("dragleave", onDragLeft);
        targetNode.addEventListener("drop", onFileDropped);
    }

    init();

    that.show = show;
    that.hide = hide;
    return that;
};