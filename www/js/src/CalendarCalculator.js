var CalendarCreator = CalendarCreator || {};
CalendarCreator.CalendarCalculator = function () {
    "use strict";
    //calculation of amount of days per month + first weekday
    
    var that = {};
    
    function getDaysOfMonth(year, month) {
        var monthStart = new Date(year, month, 1),
            monthEnd = new Date(year, month + 1, 1),
            monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
        return Math.round(monthLength); //Oktober 2016 returns 31.041666666666668 -->Math.round
    }
    
    function getFirstDay(year, month) {
        var date = new Date(year, month, 1),
            weekday;
        if (date.getDay() < 1) {
            weekday = 6;
        } else {
            weekday = date.getDay() - 1;
        }
        return weekday;
    }
    
    that.getDaysOfMonth = getDaysOfMonth;
    that.getFirstDay = getFirstDay;
    return that;
};