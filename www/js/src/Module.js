var CalendarCreator = CalendarCreator || {};
CalendarCreator.Module = function () {
    "use strict";


    //Module takes care of UI-information coming from MenuSelector and TimeSelector; creates calendarObject; CalendarController calls getCalendarObject() to get calendarObject;

    var that = new window.EventPublisher(),
        imageChecker,
        calendarCalculator,
        calendarObject = {
            resolution: Math.SQRT2,
            positioning: "imageTop",
            orientation: "portrait",

            imageSrc: "/res/images/foto/image8-big.jpg",
            imageOrientation: 1,
            rotationValue: 0,
            background: "/res/images/background/background-skulls.jpg",
            gridDesign: 6,
            weekday: 1,
            fillUpGrid: true,


            year: 2016,
            month: 2,
            days: 31,
            begin: 4,
            daysPrevious: 29,

            font: "Tempus Sans ITC",
            fontSize: "60px ", //nicht nötig
            fontStyle: "bold ",
            fontVariant: "normal ",
            uppercase: false,
            lowercase: false,
            fontColor: "rgb(62, 39, 35)",
            gridColor: "rgb(255, 152, 0)",
            highlightColor: "rgb(213, 0, 0)",

            markDays: []
        },

        timeString = "2016,2",
        daysAndBegin,
        storage;





    function saveLoval() {
        storage.save(timeString, calendarObject);
        that.notifyAll("updateUI", calendarObject);
        that.notifyAll("update");

    }

    function getCalendarObject() {
        return calendarObject;
    }


    function calculateTimeValues() {
        var previousMonthYear = calendarObject.year,
            previousMonth = calendarObject.month - 1;

        if (calendarObject.month === 0) {
            previousMonthYear = calendarObject.year - 1;
            previousMonth = 11;
        }

        if (calendarObject.month === 11) {
            previousMonthYear = calendarObject.year;
            previousMonth = 10;
        }

        calendarObject.daysPrevious = calendarCalculator.getDaysOfMonth(previousMonthYear, previousMonth);
        calendarObject.days = calendarCalculator.getDaysOfMonth(calendarObject.year, calendarObject.month);
        calendarObject.begin = calendarCalculator.getFirstDay(calendarObject.year, calendarObject.month);

        daysAndBegin = [calendarObject.days, calendarObject.begin];
    }

    function setTimeValues(event) {
        timeString = event.data[0].toString() + "," + event.data[1].toString();

        if (storage.getItem(timeString)) {
            calendarObject = storage.restore(timeString);
        } else {
            calendarObject.year = event.data[0];
            calendarObject.month = event.data[1];
        }

        calculateTimeValues();


        that.notifyAll("setHighlightUI", daysAndBegin);
        saveLoval();


    }


    function setFontValues(event) {
        var value = event.data.target,
            uncheck;

        if (calendarObject.hasOwnProperty(value.name)) {

            switch (value.name) {
            case "font":
                calendarObject[value.name] = value.innerHTML;
                break;
            case "fontStyle":
            case "fontVariant":
                if (value.checked) {
                    calendarObject[value.name] = value.value;
                } else {
                    calendarObject[value.name] = "normal ";
                }
                break;
            case "uppercase":
            case "lowercase":
                if (value.checked) {
                    if (value.name === "lowercase") {
                        uncheck = "uppercase";
                    } else {
                        uncheck = "lowercase";
                    }
                    calendarObject[value.name] = true;
                    calendarObject[uncheck] = false;
                } else {
                    calendarObject[value.name] = false;
                }
                break;
            default:
            }

            saveLoval();
        }
    }

    function setBackground(event) {
        calendarObject.background = event.data.target.src;
        saveLoval();
    }


    function setColors(event) {
        var value = event.data.target,
            valueName = value.parentElement.parentElement.id;

        if (calendarObject.hasOwnProperty(valueName)) {
            calendarObject[valueName] = value.style.backgroundColor;
            saveLoval();
        }

    }

    function setMarkedDays(event) {
        var day = parseInt(event.data, 10),
            index = calendarObject.markDays.indexOf(day);

        if (index > -1) {
            calendarObject.markDays.splice(index, 1);
        } else {
            calendarObject.markDays.push(day);
        }

        saveLoval();
    }



    function setDefaultTime() {
        var today = new Date(),
            time;

        calendarObject.year = today.getFullYear();
        calendarObject.month = today.getMonth();
        time = [calendarObject.year, calendarObject.month];

        timeString = time[0].toString() + "," + time[1].toString();


        if (storage.getItem(timeString)) {
            calendarObject = storage.restore(timeString);
        }
        calculateTimeValues();


        that.notifyAll("update");
        that.notifyAll("setHighlightUI", daysAndBegin);
        that.notifyAll("updateUI", calendarObject);
        that.notifyAll("setDefaultTimeUI", time);


    }


    function setGrid(event) {
        calendarObject.gridDesign = parseInt(event.data, 10);
        saveLoval();
    }

    function setWeekdays(event) {
        calendarObject.weekday = event.data;
        saveLoval();
    }
    
    function setFillUpGrid(event) {
        calendarObject.fillUpGrid = event.data;
        saveLoval();
    }


    function setImage(event) {
        calendarObject.imageSrc = event.data.target.src.replace("-thumbnail", "-big");
        calendarObject.imageOrientation = 0;
        calendarObject.rotationValue = 0;
        saveLoval();
    }

    function onOrientationChecked(event) {
        var imageOrientation = event;

        if (imageOrientation === 6) {
            calendarObject.rotationValue = 90;
        }
        if (imageOrientation === 8) {
            calendarObject.rotationValue = -90;
        }
        if (imageOrientation === 3) {
            calendarObject.rotationValue = 180;
        }
        saveLoval();
    }

    function setImageFromFile(event) {
        var imageFile;
        if (imageChecker.fileSupported(event.data.files[0].name) === true) {
            calendarObject.imageSrc = window.URL.createObjectURL(event.data.files[0]);
            imageFile = event.data.files[0];
            imageChecker.getOrientation(imageFile, onOrientationChecked);
            calendarObject.rotationValue = 0;
        } else {
            window.Materialize.toast("Dieses Bildformat wird nicht unterstützt. Verwende bitte eines der folgenden Bildformate: jpg, bmp, tiff, png", 10000);

        }
    }

    function setImageFromHTML(event) {
        if (imageChecker.fileSupported(event.data) === true) {
            calendarObject.imageSrc = event.data;
            saveLoval();
            //imageChecker.getOrientation(imageFile, onOrientationChecked);
            calendarObject.rotationValue = 0;
        } else {
            window.Materialize.toast("Dieses Bildformat wird nicht unterstützt. Verwende bitte eines der folgenden Bildformate: jpg, bmp, tiff, png", 10000);

        }

    }

    function setRotation(event) {
        calendarObject.rotationValue += event.data;
        saveLoval();
    }


    function onFileDropped(event) {
        var imageFile;
        if (imageChecker.fileSupported(event.data.name) === true) {
            calendarObject.imageSrc = window.URL.createObjectURL(event.data);
            imageFile = event.data;
            imageChecker.getOrientation(imageFile, onOrientationChecked);

        } else {
            window.Materialize.toast("Dieses Bildformat wird nicht unterstützt. Verwende bitte eines der folgenden Bildformate: jpg, bmp, tiff, png", 10000);

        }

    }

    function setPositioning(event) {
        calendarObject.positioning = event.data;
        saveLoval();
    }

    function setOrientation(event) {
        calendarObject.orientation = event.data;
        saveLoval();
    }
    
    function setResolution(event) {
        calendarObject.resolution = event.data;
        saveLoval();
    }

    //LOG STORAGE
    function logStorage() {
        storage.printStorage();
    }


    function init() {
        storage = new CalendarCreator.LocalStorageAdapter();
        imageChecker = new CalendarCreator.ImageChecker();
        calendarCalculator = new CalendarCreator.CalendarCalculator();
        setDefaultTime();

        return that;
    }

    that.init = init;
    that.onFileDropped = onFileDropped;
    that.setBackground = setBackground;
    that.getCalendarObject = getCalendarObject;
    that.setFontValues = setFontValues;
    that.setTimeValues = setTimeValues;
    that.setColors = setColors;
    that.setMarkedDays = setMarkedDays;
    that.setImage = setImage;
    that.setImageFromFile = setImageFromFile;
    that.setImageFromHTML = setImageFromHTML;
    that.setGrid = setGrid;
    that.setWeekdays = setWeekdays;
    that.setFillUpGrid = setFillUpGrid;
    that.setRotation = setRotation;
    that.setPositioning = setPositioning;
    that.setOrientation = setOrientation;
    that.setResolution = setResolution;


    //LOG STORAGE
    that.logStorage = logStorage;
    return that;

};