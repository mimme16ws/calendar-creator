var CalendarCreator = CalendarCreator || {};
CalendarCreator.MenuSelector = function () {
    "use strict";

    //takes care of all UI-events concerning the menu

    var that = new window.EventPublisher(),
        menu = document.querySelector(".menu"),
        submenu = document.querySelectorAll(".submenu"),
        submenuContainer = document.querySelector(".submenu-container"),

        submenuOrientation = document.querySelectorAll(".dropdown-content")[0],
        submenuOrientationButton = document.querySelectorAll(".dropdown-button")[0],

        submenuPositioning = document.querySelectorAll(".dropdown-content")[1],
        submenuPositioningButton = document.querySelectorAll(".dropdown-button")[1],


        submenuGrid = document.querySelector(".grid-design"),
        gridDesigns = document.querySelectorAll(".grid-image"),
        submenuGridWeekdays = document.querySelector("#input-weekdays"),
        submenuGridFillUpGrid = document.querySelector("#input-fill-up-grid"),

        submenuImage = document.querySelector(".image"),
        imageThumbnails = document.querySelectorAll(".calendar-image"),
        htmlImageSubmitButton = document.querySelector("#html-image-submit"),
        fileImageSubmitButton = document.querySelector("#file-image-submit"),
        rotateLeftButton = document.querySelector("#icon-rotate-left"),
        rotateRightButton = document.querySelector("#icon-rotate-right"),

        submenuHighlight = document.querySelector(".highlight"),


        submenuColor = document.querySelector(".color-palette"),
        colors = document.querySelectorAll(".color"),
        submenuDownloadButton = document.querySelector("#download-button"),
        downloadResolution = document.querySelector("#form-resolution"),

        submenuFont = document.querySelectorAll(".dropdown-content")[2],
        submenuFontButton = document.querySelectorAll(".dropdown-button")[2],
        submenuFontItalic = document.querySelector("#input-italic"),
        submenuFontBold = document.querySelector("#input-bold"),
        submenuFontUppercase = document.querySelector("#input-uppercase"),
        submenuFontLowercase = document.querySelector("#input-lowercase"),
        submenuBackground = document.querySelector(".background"),
        backgroundThumbnails = document.querySelectorAll(".background-image"),
        submenuPrint = document.querySelector("#icon-10"),

        begin,
        previousTime = "",
        fileType = "";





    function closeSubmenus() {
        var i;
        for (i = 0; i < submenu.length; i++) {
            if (submenu[i].style.display === "block") {
                window.$(submenu[i]).fadeOut(700);
            }
        }
    }

    function onMenuClicked(event) {
        if (document.querySelector("#submenu-" + event.target.id).style.display === "none") {
            closeSubmenus();
            window.$("#submenu-" + event.target.id).slideDown();
            document.querySelector("#submenu-" + event.target.id).style.display = "block";
        } else {
            window.$("#submenu-" + event.target.id).fadeOut(400);
        }

    }

    function onDocumentClicked(event) {
        var i;
        for (i = 0; i < submenu.length; i++) {
            if (event.target !== submenuContainer && !submenuContainer.contains(event.target)) {
                closeSubmenus();
            }
        }

    }


    function setOrientationUI(orientation) {
        if (orientation === "portrait") {
            submenuOrientationButton.innerHTML = "<div class= \"waves-effect waves-light\" style=\" height: 7vh; width: 7vh;  margin-right: 10px;\"> <img  id =\"dropdown-button-img\" style =\"height: 100%; width: 100%;\" src=\"res/images/icon/ic_portrait_orientation.png\" /></div>Hochformat";
            window.$(".submenu").css("background", "transparent");
            
                
        } else {
            submenuOrientationButton.innerHTML = "<div class= \"waves-effect waves-light\" style=\" height: 7vh; width: 7vh;  margin-right: 10px;\"> <img  id =\"dropdown-button-img\" style =\"height: 100%; width: 100%;\" src=\"res/images/icon/ic_landscape_orientation.png\" /></div>Querformat";
            window.$(".submenu").css("background", "rgba(250,250,250, 0.8)");
        }
    }

    function onOrientationClicked(event) {
        var value = event.target.getAttribute("value");
        that.notifyAll("orientationChanged", value);
    }

    function setPositioningUI(position) {
        if (position === "imageTop") {
            submenuPositioningButton.innerHTML = "<div class= \"waves-effect waves-light\" style=\" height: 7vh; width: 7vh;  margin-right: 10px;\"> <img  id =\"dropdown-button-img\" style =\"height: 100%; width: 100%;\" src=\"res/images/icon/ic_image_top.png\" /></div>Bild oben";
        } else {
            submenuPositioningButton.innerHTML = "<div class= \"waves-effect waves-light\" style=\" height: 7vh; width: 7vh;  margin-right: 10px;\"> <img  id =\"dropdown-button-img\" style =\"height: 100%; width: 100%;\" src=\"res/images/icon/ic_image_bottom.png\" /></div>Bild unten";
        }
    }


    function onPositioningClicked(event) {
        var value = event.target.getAttribute("value");
        that.notifyAll("positioningChanged", value);
    }

    function setBackgroundUI(list, value) {
        var i;
        for (i = 0; i < list.length; i++) {
            list[i].style.border = "0.3vh solid #616161";
            if (list[i].src.split("images")[1] === value.split("images")[1]) {
                list[i].style.border = "0.74vh solid #000 ";
            }
        }
    }

    function onBackgroundClicked(event) {
        if (event.target.parentElement.parentElement.className === "row") {
            setBackgroundUI(backgroundThumbnails, event.target.src);
            that.notifyAll("backgroundChanged", event);
        }
    }

    function setGridUI(value) {
        var i;
        for (i = 0; i < gridDesigns.length; i++) {
            gridDesigns[i].style.border = "transparent";
        }
        gridDesigns[value - 1].style.border = "0.74vh solid #000 ";
    }


    function onGridClicked(event) {
        var value = parseInt(event.target.parentElement.getAttribute("value"), 10);
        if (event.target.tagName === "IMG") {
            setGridUI(value);
            that.notifyAll("gridChanged", value);
        }
    }

    function setGridOptionsUI(option, checked) {
        if (checked === 1 || checked === true) {
            //window.$("#input-weekdays")[0].checked = true;
            option.checked = true;
        } else {
            //window.$("#input-weekdays")[0].checked = false;
            option.checked = false;
        }
    }

    function onWeekdaysClicked(event) {
        var checked = 0;
        if (event.target.checked) {
            checked = 1;
        }
        that.notifyAll("weekdaysChanged", checked);
    }

    function onFillUpGridClicked(event) {
        var checked = false;
        if (event.target.checked) {
            checked = true;
        }
        that.notifyAll("fillUpGridChanged", checked);
    }

    function setImageUI(value) {
        var i;
        for (i = 0; i < imageThumbnails.length; i++) {
            imageThumbnails[i].style.border = "0.3vh solid #616161";

            if (imageThumbnails[i].src.split("foto")[1] === value.split("foto")[1]) {
                imageThumbnails[i].style.border = "0.74vh solid #000 ";
            }

        }
    }

    function onImageClicked(event) {
        if (event.target.tagName === "IMG" && event.target.parentElement.tagName === "DIV") {
            that.notifyAll("imageChanged", event);
        }
    }


    function setColorUIBorderColor(currentColor, value, i) {
        if (currentColor === value) {
            if (value === "rgb(0, 0, 0)") {
                colors[i].style.border = "0.74vh solid #808080 ";
            } else {
                colors[i].style.border = "0.74vh solid #000 ";
            }
        }
    }

    function setColorUI(value, valueParentId) {
        var i;
        switch (valueParentId) {
        case "fontColor":
            for (i = 0; i < colors.length / 3; i++) {
                colors[i].style.border = "0px ";
                setColorUIBorderColor(colors[i].style.backgroundColor, value, i);
            }
            break;
        case "gridColor":
            for (i = colors.length / 3; i < colors.length * 2 / 3; i++) {
                colors[i].style.border = "0px ";
                setColorUIBorderColor(colors[i].style.backgroundColor, value, i);
            }
            break;
        case "highlightColor":
            for (i = colors.length * 2 / 3; i < colors.length; i++) {
                colors[i].style.border = "0px ";
                setColorUIBorderColor(colors[i].style.backgroundColor, value, i);
            }
            break;
        default:
        }
    }

    function onColorClicked(event) {
        if (event.target.parentElement.className === "row") {
            setColorUI(event.target.style.backgroundColor, event.target.parentElement.parentElement.id);
            that.notifyAll("colorChanged", event);
        }
    }

    function setHighlightUI(highlightDays, value) {
        highlightDays[value].style.background = "#ffa726";
    }

    function onHighlightClicked(event) {
        var value = parseInt(event.target.innerText, 10),
            highlightDays = document.querySelectorAll(".day");

        highlightDays[value + begin - 1].style.background = "#d7ccc8 ";
        that.notifyAll("highlightedDaysChanged", value);
    }


    function createSubmenuHighlight(event) {
        var days = event.data[0],
            dayNum = 1,
            dayItem,
            i,
            row = document.createElement("div");

        begin = event.data[1];

        //clear submenu
        while (submenuHighlight.childNodes.length > 2) {
            submenuHighlight.removeChild(submenuHighlight.lastChild);
        }

        row.setAttribute("class", "row");

        //create hidden elements
        for (i = 0; i < begin; i++) {
            dayItem = document.createElement("div");
            dayItem.setAttribute("class", "col day hvr-shrink waves-effect waves-light");
            dayItem.setAttribute("style", "visibility: hidden");
            row.appendChild(dayItem);
        }

        //create actual elements
        while (i < days + begin) {
            if (i % 7 === 0) {
                submenuHighlight.appendChild(row);
                row = document.createElement("div");
                row.setAttribute("class", "row");
            }
            dayItem = document.createElement("div");

            dayItem.setAttribute("class", "col day hvr-shrink waves-effect waves-light");
            dayItem.style.background = "#d7ccc8";
            dayItem.innerHTML = dayNum;

            dayNum++;
            row.appendChild(dayItem);
            i++;
        }
        submenuHighlight.appendChild(row);



    }


    function setFontUI(font) {
        submenuFontButton.style.fontFamily = font;
        submenuFontButton.innerHTML = font;
    }

    function onFontClicked(event) {
        var font = event.target.innerHTML;
        setFontUI(font);
        that.notifyAll("fontChanged", event);
    }

    function setFontStyle(style, value) {
        if (value === "bold ") {
            style.checked = true;
        } else if (value === "italic ") {
            style.checked = true;
        } else if (value === true) {
            style.checked = true;
        } else {
            style.checked = false;
        }
    }

    function onItalicClicked(event) {
        that.notifyAll("fontVariantChanged", event);
    }

    function onBoldClicked(event) {
        that.notifyAll("fontStyleChanged", event);
    }

    function onUppercaseClicked(event) {
        that.notifyAll("fontUppercaseChanged", event);
    }

    function onLowercaseClicked(event) {
        that.notifyAll("fontLowercaseChanged", event);
    }



    function onHtmlButtonClicked() {
        var i;
        for (i = 0; i < imageThumbnails.length; i++) {
            imageThumbnails[i].style.border = "0.3vh solid #616161";
        }
        that.notifyAll("newHTMLImage", window.$("#textarea1")[0].value);
    }

    function onFileButtonClicked() {
        var i;
        for (i = 0; i < imageThumbnails.length; i++) {
            imageThumbnails[i].style.border = "0.3vh solid #616161";
        }
        that.notifyAll("newFileImage", window.$("input[type=file]")[0]);
    }

    function onRotateLeftClicked() {
        that.notifyAll("rotateLeft", -90);
    }

    function onRotateRightClicked() {
        that.notifyAll("rotateLeft", 90);
    }

    function onPrintClicked() {
        that.notifyAll("print");
    }

    function onDownloadButtonClicked() {
        if (window.$("#png")[0].checked === true) {
            fileType = "png";
        } else if (window.$("#jpg")[0].checked === true) {
            fileType = "jpg";
        }

        that.notifyAll("download", [this, fileType]);


    }


    function checkMarkedDays(currentDay, markedDays) {
        return markedDays.some(function (markedDay) {
            return currentDay === markedDay;
        });
    }

    function onResolutionClicked(event) {
        var resolution;
        if (event.target.id === "din-a5") {
            resolution = 1;
        } else if (event.target.id === "din-a4") {
            resolution = Math.sqrt(2);
        } else {
            resolution = 2;
        }

        that.notifyAll("resolutionChanged", resolution);
    }

    function setResolutionUI(value) {
        if (value === 1) {
            window.$("#din-a5")[0].checked = true;
        } else if (value === 2) {
            window.$("#din-a3")[0].checked = true;
        } else {
            window.$("#din-a4")[0].checked = true;
        }
    }

    //adjusts UI to data from localstorage
    function updateUI(event) {
        var calendarObject = event.data,
            currentTime = calendarObject.year.toString() + "," + calendarObject.month.toString(),
            imageSourceType,
            highlightDays,
            i;
        if (currentTime !== previousTime) {
            //just need to be checked if time is changed
            setGridOptionsUI(submenuGridWeekdays, calendarObject.weekday);
            setGridOptionsUI(submenuGridFillUpGrid, calendarObject.fillUpGrid);
            setFontStyle(submenuFontItalic, calendarObject.fontVariant);
            setFontStyle(submenuFontBold, calendarObject.fontStyle);

            setResolutionUI(calendarObject.resolution);

            previousTime = currentTime;
        }

        setFontStyle(submenuFontUppercase, calendarObject.uppercase);
        setFontStyle(submenuFontLowercase, calendarObject.lowercase);
        setOrientationUI(calendarObject.orientation);
        setPositioningUI(calendarObject.positioning);
        setBackgroundUI(backgroundThumbnails, calendarObject.background);
        setGridUI(calendarObject.gridDesign);

        imageSourceType = calendarObject.imageSrc.split(":")[0];

        if (calendarObject.imageSrc.indexOf("res/images/foto/image") > -1) {
            setImageUI(calendarObject.imageSrc.replace("-big", "-thumbnail"));
        } else if (imageSourceType === "http") {
            window.$("#textarea1")[0].innerHTML = calendarObject.imageSrc;

        }

        setColorUI(calendarObject.fontColor, "fontColor");
        setColorUI(calendarObject.gridColor, "gridColor");
        setColorUI(calendarObject.highlightColor, "highlightColor");



        //uiHighlighDays
        highlightDays = document.querySelectorAll(".day");

        for (i = 0; i < calendarObject.days + 1; i++) {
            if (checkMarkedDays(i, calendarObject.markDays)) {
                setHighlightUI(highlightDays, i + calendarObject.begin - 1);
            }

        }

        //UIFONT
        setFontUI(calendarObject.font);

    }


    function initEvents() {

        menu.addEventListener("click", onMenuClicked, false);
        window.addEventListener("mousedown", onDocumentClicked, false);
        submenuPositioning.addEventListener("click", onPositioningClicked, false);
        submenuOrientation.addEventListener("click", onOrientationClicked, false);
        submenuGrid.addEventListener("click", onGridClicked, false);
        submenuGridWeekdays.addEventListener("click", onWeekdaysClicked, false);
        submenuGridFillUpGrid.addEventListener("click", onFillUpGridClicked, false);

        submenuImage.addEventListener("click", onImageClicked, false);
        htmlImageSubmitButton.addEventListener("click", onHtmlButtonClicked, false);
        fileImageSubmitButton.addEventListener("click", onFileButtonClicked, false);
        rotateLeftButton.addEventListener("click", onRotateLeftClicked, false);
        rotateRightButton.addEventListener("click", onRotateRightClicked, false);

        submenuHighlight.addEventListener("mousedown", onHighlightClicked, false);

        submenuColor.addEventListener("click", onColorClicked, false);
        submenuDownloadButton.addEventListener("click", onDownloadButtonClicked, false);
        downloadResolution.addEventListener("change", onResolutionClicked, false);

        submenuFont.addEventListener("click", onFontClicked, false);
        submenuFontItalic.addEventListener("click", onItalicClicked, false);
        submenuFontBold.addEventListener("click", onBoldClicked, false);
        submenuFontUppercase.addEventListener("click", onUppercaseClicked, false);
        submenuFontLowercase.addEventListener("click", onLowercaseClicked, false);
        submenuBackground.addEventListener("click", onBackgroundClicked, false);

        submenuPrint.addEventListener("click", onPrintClicked, false);


    }


    function init() {
        initEvents();

        return that;
    }

    that.init = init;
    that.createSubmenuHighlight = createSubmenuHighlight;
    that.updateUI = updateUI;
    return that;
};