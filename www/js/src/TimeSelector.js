var CalendarCreator = CalendarCreator || {};
CalendarCreator.TimeSelector = function () {
    "use strict";

    //takes care of all UI-events concerning the time-menu on the left

    var that = new window.EventPublisher(),
        yearInput = document.querySelector("#year-input"),
        year = document.querySelector("#year"),
        yearString,
        widgetFront = document.getElementsByClassName("widget-content front")[0],
        widget = document.getElementsByClassName("widget")[0],
        monthsContainer = document.querySelector(".months-container"),
        months = document.querySelectorAll(".month"),
        front = true,
        time = [2010, 1];




    //flip animation
    function flip(isFront, inputDisabled) {
        widget.classList.toggle("flipped");
        yearInput.disabled = inputDisabled;
        front = isFront;

    }

    function onWidgetClicked() {
        flip(false, false);
        yearInput.value = "";
        yearInput.select();

    }

    function submit() {
        if (front === false) {
            if (yearInput.value === "") {
                yearString = year.innerText;
            } else {
                yearString = yearInput.value;
            }


            year.innerText = yearString;
            time[0] = parseInt(yearString, 10);
            flip(true, true);
            that.notifyAll("yearChanged", time);

        }
    }

    function checkKey(event) {
        if (event.keyCode === 13) {
            submit();
        }

        if (event.keyCode === 27 && front === false) {
            flip(true, true);
        }
    }

    function onMonthClicked(event) {

        var i;
        for (i = 0; i < months.length; i++) {
            months[i].setAttribute("style", "background-color: #d7ccc8");
        }

        event.target.style.background = "#ffa726 ";
        time[1] = parseInt(event.target.getAttribute("value"), 10);
        that.notifyAll("monthChanged", time);

    }


    function setDefaultTimeUI(event) {
        time = event.data;
        year.innerText = time[0].toString();
        months[time[1]].setAttribute("style", "background-color: #ffa726");

    }

    function initEvents() {
        widgetFront.addEventListener("click", onWidgetClicked, false);
        window.addEventListener("keyup", checkKey, false);
        window.addEventListener("mouseup", submit, false);
        monthsContainer.addEventListener("mousedown", onMonthClicked, false);

    }


    function init() {
        initEvents();
        return that;
    }

    that.init = init;
    that.setDefaultTimeUI = setDefaultTimeUI;

    return that;
};