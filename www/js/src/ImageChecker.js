var CalendarCreator = CalendarCreator || {};
CalendarCreator.ImageChecker = function () {
    "use strict";
    //checks if droped Files are supported by canvas; checks orientation of foto
    var that = {},
        supportedImageFiles = ["jpg", "jpeg", "bmp", "tiff", "png"];



    function getOrientation(file, callback) {
        //stackoverflow http://stackoverflow.com/questions/7584794/accessing-jpeg-exif-rotation-data-in-javascript-on-the-client-side
        var reader = new window.FileReader();
        reader.onload = function (e) {
            
            var view = new window.DataView(e.target.result),
                length,
                marker,
                offset,
                little,
                tags,
                i;
            if (view.getUint16(0, false) !== 0xFFD8) {
                return callback(-2);
            }
            length = view.byteLength;
            offset = 2;
            while (offset < length) {
                marker = view.getUint16(offset, false);
                offset += 2;
                if (marker === 0xFFE1) {
                    if (view.getUint32(offset += 2, false) !== 0x45786966) {
                        return callback(-1);
                    }
                    little = view.getUint16(offset += 6, false) === 0x4949;
                    offset += view.getUint32(offset + 4, little);
                    tags = view.getUint16(offset, little);
                    offset += 2;
                    for (i = 0; i < tags; i++) {
                        if (view.getUint16(offset + (i * 12), little) === 0x0112) {
                            return callback(view.getUint16(offset + (i * 12) + 8, little));
                        }
                    }
                } else if ((marker && 0xFF00) !== 0xFF00) {
                    break;
                } else {
                    offset += view.getUint16(offset, false);
                }
            }
            return callback(-1);
        };
        reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
    }


    function fileSupported(src) {
        var strings = src.split("."),
            file = strings[strings.length - 1].toLowerCase().replace(/\s+/g, ""),
            i;

        for (i = 0; i < supportedImageFiles.length; i++) {
            if (file === supportedImageFiles[i]) {
                return true;
            }
        }
        return false;

    }

    that.getOrientation = getOrientation;
    that.fileSupported = fileSupported;
    return that;
};